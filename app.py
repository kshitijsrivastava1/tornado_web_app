import argparse
import traceback


import tornado.web
import tornado.ioloop

import handlers
import logging
 # Configuring logging to show timestamps
logging.basicConfig(format='[%(asctime)s] p%(thread)s {%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG
                    )

class Application(tornado.web.RequestHandler):
    def __init__(self, config, port):
        app = tornado.web.Application([
            (r"/", handlers.common.rootRequest),
            (r"/blog", handlers.common.blogRequestHandler),
            (r"/isEven", handlers.api.queryStringRequestHandler),
        ])
        app.listen(port)
        print("I'm listening on port " , port)


async def init():
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=8882)
    args = parser.parse_args()

    # Initialize state and static resources
    config = tornado.ioloop.IOLoop.current().run_sync(init)
    # Start application
    application = Application(config, args.port)
    try:
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        pass
    except Exception:
        traceback.print_exc()
    finally:
        tornado.ioloop.IOLoop.current().stop()
