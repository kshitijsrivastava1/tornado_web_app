import tornado.web
import tornado.ioloop

class queryStringRequestHandler(tornado.web.RequestHandler):
    def get(self):
        n = int(self.get_argument("n"))
        r = "odd" if n % 2 else "even"
        self.write("the number " + str(n) + " is " + r)
