import tornado.web
import tornado.ioloop

class rootRequest(tornado.web.RequestHandler):
    async def get(self):
        self.render("../templates/index.html")

class blogRequestHandler(tornado.web.RequestHandler):
    async def get(self):
        self.render("../templates/blog.html")
